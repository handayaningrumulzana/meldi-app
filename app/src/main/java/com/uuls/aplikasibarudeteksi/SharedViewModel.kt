package com.uuls.aplikasibarudeteksi

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class SharedViewModel : ViewModel() {
    // LiveData untuk URL gambar
    private val _imageUrl = MutableLiveData<String>()
    val imageUrl: LiveData<String>
        get() = _imageUrl

    // LiveData untuk hasil pemindaian
    private val _result = MutableLiveData<String>()
    val result: LiveData<String>
        get() = _result

    // LiveData untuk tingkat keyakinan hasil pemindaian
    private val _confidence = MutableLiveData<Double>()
    val confidence: LiveData<Double>
        get() = _confidence

    // Metode untuk memperbarui URL gambar
    fun updateImageUrl(url: String) {
        _imageUrl.value = url
    }

    // Metode untuk memperbarui hasil pemindaian dan tingkat keyakinannya
    fun updateResult(result: String, confidence: Double) {
        _result.value = result
        _confidence.value = confidence
    }
}


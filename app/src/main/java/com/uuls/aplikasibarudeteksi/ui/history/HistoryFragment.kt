package com.uuls.aplikasibarudeteksi.ui.history

import HistoryItem
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.uuls.aplikasibarudeteksi.R

class HistoryFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var historyAdapter: HistoryAdapter
    private lateinit var historyItems: MutableList<HistoryItem>
    private lateinit var db: FirebaseFirestore
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_history, container, false)

        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        historyItems = mutableListOf()
        historyAdapter = HistoryAdapter(requireContext(), historyItems)
        recyclerView.adapter = historyAdapter

        db = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()

        loadHistory()

        return view
    }

    private fun loadHistory() {
        val userId = auth.currentUser?.uid ?: return
        db.collection("history")
            .whereEqualTo("userId", userId)
            .get()
            .addOnSuccessListener { documents ->
                historyItems.clear()
                for (document in documents) {
                    val historyItem = document.toObject(HistoryItem::class.java)
                    historyItems.add(historyItem)
                }
                historyAdapter.notifyDataSetChanged()
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }

    companion object {
        private const val TAG = "HistoryFragment"
    }
}

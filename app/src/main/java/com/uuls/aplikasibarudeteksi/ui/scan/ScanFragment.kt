package com.uuls.aplikasibarudeteksi.ui.scan

import HistoryItem
import android.Manifest
import android.content.ActivityNotFoundException
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.uuls.aplikasibarudeteksi.R
import com.uuls.aplikasibarudeteksi.ml.ModelUnquant
import org.tensorflow.lite.DataType
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.text.SimpleDateFormat
import java.util.*

class ScanFragment : Fragment() {

    private lateinit var result: TextView
    private lateinit var confidence: TextView
    private lateinit var imageView: ImageView
    private lateinit var picture: Button
    private lateinit var galleryButton: Button
    private val imageSize = 224  // Ukuran gambar tetap untuk pemrosesan

    // Firebase untuk penyimpanan dan database
    private lateinit var storageReference: StorageReference
    private lateinit var db: FirebaseFirestore
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_scan, container, false)

        // Menghubungkan variabel dengan elemen di layout
        result = view.findViewById(R.id.result)
        confidence = view.findViewById(R.id.confidence)
        imageView = view.findViewById(R.id.imageView)
        picture = view.findViewById(R.id.button)
        galleryButton = view.findViewById(R.id.button_gallery)

        // Menginisialisasi referensi Firebase untuk penyimpanan dan database
        storageReference = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance()
        auth = FirebaseAuth.getInstance()  // Menginisialisasi FirebaseAuth

        // Menetapkan listener untuk tombol kamera
        picture.setOnClickListener {
            // Mengecek izin kamera
            if (ContextCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.CAMERA
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                // Membuka kamera jika izin diberikan
                openCamera()
            } else {
                // Meminta izin kamera jika belum diberikan
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.CAMERA),
                    100
                )
            }
        }

        // Menetapkan listener untuk tombol galeri
        galleryButton.setOnClickListener {
            openGallery()
        }

        // Mengembalikan tampilan yang telah diinisialisasi
        return view
    }

    // Fungsi untuk membuka kamera
    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { cameraIntent ->
            // Memastikan ada aplikasi kamera yang dapat menangani intent
            cameraIntent.resolveActivity(requireContext().packageManager)?.let {
                // Memulai aktivitas pengambilan gambar
                startActivityForResult(cameraIntent, 1)
            }
        }
    }

    // Fungsi untuk membuka galeri dan memungkinkan pengguna memilih gambar
    private fun openGallery() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        galleryIntent.type = "image/*"
        try {
            startActivityForResult(galleryIntent, 2)
        } catch (e: ActivityNotFoundException) {
            // Tangani pengecualian jika aktivitas tidak ditemukan
        }
    }

    // Fungsi engklasifikasikan gambar menggunakan model machine learning
    private fun classifyImage(image: Bitmap) {
        try {
            val model = ModelUnquant.newInstance(requireContext())

            // Membuat input tensor dengan ukuran tetap
            val inputFeature0 = TensorBuffer.createFixedSize(intArrayOf(1, 224, 224, 3), DataType.FLOAT32)
            val byteBuffer = ByteBuffer.allocateDirect(4 * imageSize * imageSize * 3)
            byteBuffer.order(ByteOrder.nativeOrder())

            // Mengubah piksel gambar menjadi float dan memasukkannya ke dalam byte buffer
            val intValues = IntArray(imageSize * imageSize)
            image.getPixels(intValues, 0, image.width, 0, 0, image.width, image.height)
            var pixel = 0
            for (i in 0 until imageSize) {
                for (j in 0 until imageSize) {
                    val value = intValues[pixel++]
                    byteBuffer.putFloat(((value shr 16) and 0xFF) * (1f / 255f))
                    byteBuffer.putFloat(((value shr 8) and 0xFF) * (1f / 255f))
                    byteBuffer.putFloat((value and 0xFF) * (1f / 255f))
                }
            }

            // Memuat buffer ke input tensor
            inputFeature0.loadBuffer(byteBuffer)

            // Memproses input dengan model
            val outputs = model.process(inputFeature0)
            val outputFeature0 = outputs.getOutputFeature0AsTensorBuffer()

            // Mengambil nilai kepercayaan dari output
            val confidences = outputFeature0.floatArray

            // Menemukan kelas dengan nilai kepercayaan tertinggi
            var maxPos = 0
            var maxConfidence = 0f
            for (i in confidences.indices) {
                if (confidences[i] > maxConfidence) {
                    maxConfidence = confidences[i]
                    maxPos = i
                }
            }

            // Mendefinisikan kelas-kelas yang mungkin
            val classes = arrayOf("Sehat", "Kutu Kebul", "Layu Fusarium", "Thrips", "Bukan Daun Melon")
            if (maxPos < classes.size) {
                val classifiedClass = classes[maxPos]
                result.text = classifiedClass
                if (classifiedClass == "Bukan Daun Melon") {
                    confidence.text = ""
                } else {
                    confidence.text = String.format("%s: %.1f%%", classifiedClass, maxConfidence * 100)
                }
            } else {
                result.text = ""
                confidence.text = ""
            }

            // Menutup model untuk mengosongkan sumber daya
            model.close()
        } catch (e: IOException) {
            // Tangani pengecualian jika terjadi kesalahan I/O
        }
    }

    // Fungsi yang dipanggil saat aktivitas lain selesai dan mengembalikan hasil
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK) {
            when (requestCode) {
                1 -> {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    val dimension = imageBitmap.width.coerceAtMost(imageBitmap.height)
                    val thumbnail = ThumbnailUtils.extractThumbnail(imageBitmap, dimension, dimension)
                    imageView.setImageBitmap(thumbnail)
                    val scaledImage = Bitmap.createScaledBitmap(thumbnail, imageSize, imageSize, false)
                    classifyImage(scaledImage)
                    uploadImageToFirestore(thumbnail)
                }

                2 -> {
                    val selectedImageUri = data?.data
                    val imageBitmap = BitmapFactory.decodeStream(selectedImageUri?.let {
                        requireActivity().contentResolver.openInputStream(it)
                    })
                    imageView.setImageBitmap(imageBitmap)
                    val scaledImage = Bitmap.createScaledBitmap(imageBitmap, imageSize, imageSize, false)
                    classifyImage(scaledImage)
                    uploadImageToFirestore(imageBitmap)
                }
            }
        }
    }

    // Fungsi untuk mengunggah gambar ke Firebase Storage dan menyimpan data ke Firestore
    private fun uploadImageToFirestore(bitmap: Bitmap) {
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val fileName = "images/IMG_$timeStamp.jpg"
        val imageRef = storageReference.child(fileName)

        val uploadTask = imageRef.putBytes(data)
        uploadTask.addOnSuccessListener {
            imageRef.downloadUrl.addOnSuccessListener { uri ->
                val imageUrl = uri.toString()
                val resultText = result.text.toString()
                val confidenceText = confidence.text.toString()

                // Simpan data ke Firestore dengan menggunakan timestamp sebagai ID
                saveToFirestore(imageUrl, resultText, confidenceText, timeStamp)

                Toast.makeText(requireContext(), "Hasil Berhasil Disimpan", Toast.LENGTH_SHORT).show()
            }
        }.addOnFailureListener {
            Toast.makeText(requireContext(), "Image Upload Failed", Toast.LENGTH_SHORT).show()
        }
    }

    // Fungsi untuk menyimpan data ke Firestore dengan timestamp sebagai ID
    private fun saveToFirestore(imageUrl: String, result: String, confidence: String, timeStamp: String) {
        val historyRef = db.collection("history")

        // Mendapatkan ID pengguna yang masuk
        val userId = auth.currentUser?.uid ?: "unknown"

        val historyData = hashMapOf(
            "imageUrl" to imageUrl,
            "result" to result,
            "confidence" to confidence,
            "userId" to userId // Menambahkan ID pengguna ke dalam data
        )

        // Simpan data dengan timestamp sebagai ID
        historyRef.document(timeStamp)
            .set(historyData)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: $timeStamp")
            }
            .addOnFailureListener { e ->
                Log.e(TAG, "Error adding document", e)
            }
    }
}

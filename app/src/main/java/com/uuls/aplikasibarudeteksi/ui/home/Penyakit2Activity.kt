package com.uuls.aplikasibarudeteksi.ui.home

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.uuls.aplikasibarudeteksi.R

class Penyakit2Activity : AppCompatActivity() {
    private val db = Firebase.firestore

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_penyakit2)

//        val textViewNamaPenyakit: TextView = findViewById(R.id.textViewNamaPenyakit)
        val textViewCiriCiri: TextView = findViewById(R.id.textViewCiriCiri)
        val textViewPenanganan: TextView = findViewById(R.id.textViewPenanganan)

        // mengambil data dari Firestore
        val docRef = db.collection("data_penyakit").document("S4VZoAR5Eyh0S5TjwQuE")
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val namaPenyakit = document.getString("nama_penyakit")
                    val ciriCiri = document.getString("ciri_ciri")
                    val penanganan = document.getString("penanganan")

                    // menampilkan data di TextView
//                    textViewNamaPenyakit.text = namaPenyakit
                    textViewCiriCiri.text = ciriCiri
                    textViewPenanganan.text = penanganan
                } else {
                    Log.d("TAG", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("TAG", "get failed with ", exception)
            }
    }
}

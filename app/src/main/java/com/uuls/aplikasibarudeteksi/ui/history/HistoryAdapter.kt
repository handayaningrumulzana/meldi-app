package com.uuls.aplikasibarudeteksi.ui.history

import HistoryItem
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.uuls.aplikasibarudeteksi.R

class HistoryAdapter(private val context: Context, private val historyItems: List<HistoryItem>) :
    RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_history, parent, false)
        return HistoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        val historyItem = historyItems[position]
        Glide.with(context)
            .load(historyItem.imageUrl)
            .placeholder(R.drawable.rounded_box)
            .into(holder.imageView)
        holder.resultTextView.text = historyItem.result
        holder.confidenceTextView.text = historyItem.confidence
    }

    override fun getItemCount(): Int {
        return historyItems.size
    }

    inner class HistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.item_image)
        val resultTextView: TextView = itemView.findViewById(R.id.item_result)
        val confidenceTextView: TextView = itemView.findViewById(R.id.item_confidence)
    }
}



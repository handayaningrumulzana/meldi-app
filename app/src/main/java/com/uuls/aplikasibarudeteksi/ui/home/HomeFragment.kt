package com.uuls.aplikasibarudeteksi.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.uuls.aplikasibarudeteksi.R

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        val frameLayout1: View = view.findViewById(R.id.frameLayout1)
        val frameLayout2: View = view.findViewById(R.id.frameLayout2)
        val frameLayout3: View = view.findViewById(R.id.frameLayout3)

        frameLayout1.setOnClickListener {
            // Saat frameLayout1 diklik, buka Penyakit1Activity
            val intent = Intent(activity, Penyakit1Activity::class.java)
            startActivity(intent)
        }

        frameLayout2.setOnClickListener {
            // Saat frameLayout1 diklik, buka Penyakit1Activity
            val intent = Intent(activity, Penyakit2Activity::class.java)
            startActivity(intent)
        }

        frameLayout3.setOnClickListener {
            // Saat frameLayout1 diklik, buka Penyakit1Activity
            val intent = Intent(activity, Penyakit3Activity::class.java)
            startActivity(intent)
        }

        return view
    }
}

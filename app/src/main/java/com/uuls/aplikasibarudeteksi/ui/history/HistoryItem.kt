data class HistoryItem(
    val imageUrl: String = "",
    val result: String = "",
    val confidence: String = "",
    val userId: String = ""
)
